from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'student.views.home', name='home'),
      url(r'^$', views.Home_page,name='homepage'),
      url(r'^student/$',views.attendence,name='attendence'),
      url(r'^student/$',views.behavior,name='behavior'),
    # url(r'^student/', include('student.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
