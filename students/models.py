from django.db import models

# Create your models here.
class UserAccount(model.Model):
    Student_id = models.IntegerField(max_length=10)
    name = models.CharField(max_length=30)
    Age = models.IntegerField(max_length=3)
    Class = models.CharField(max_length=20)
    username = userAccount.objects.get(name)

    def __unicode__(self):
        return "%s" %(self.name)

class Attendance(model.Model):
    Id = models.IntegerField(max_length=10)
    Student_id = models.IntegerField(max_length=10)
    Date =  models.DateField()
    Time = models.DateTimeField(default=datetime.now())
    attendance = Attendance.objects.get(att)
    return att

class Points(models.Model):
    Id = models.IntegerField(max_length=10)
    Student_id = models.IntegerField(max_length=10)
    points = models.IntegerField(max_length=10)

class Behaviour(model.Models):
    Id = models.Integer(max_length=6)
    BehaviourName = models.CharField(max_length=20)
    points = models.IntegerField(max_length=10)

    def __unicode__(self):
        return "%s" %(self.behaviour)
    
    def get_fields(self):
            return [(field.name,field.value_to_string(self)) for field in behaviour._meta.fields]
